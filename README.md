# README #

Not an application. Just random bits of code and scripts.

### What is this repository for? ###

* Quick summary: I will dump random scripts and stuff here so I can find them later in one central place. You can take them and use them if you like, but if they somehow break everything you own it's probably mostly your own fault.

### How do I get set up? ###

* Summary of set up: Pull repo, copy what you want, figure out what it actually does, figure out what you actually want to do, do what you want with it, done.
* Dependencies: Shell(s), programming environments, IDEs, I don't really know, it could get pretty messy in here.
* Database configuration: Maybe, probably.
* How to run tests: Run on a live system supporting at least one million users a day without any pre-test, peer review, application of staging requirements, or bureaucratic change processes.
* Deployment instructions: See "How to run tests"

### Contribution guidelines ###

* Writing tests: Please don't waste your time. You could be curing cancer or inventing a delicious snack.
* Code review: See "Writing tests"
* Other guidelines: See "Writing tests"

### Who do I talk to? ###

* Repo owner or admin: Please (don't) contact voutasaurus. 
* Other community or team contact: I shudder to think what kind of "community" or "team" would want to answer your questions about this repo.