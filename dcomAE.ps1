################################################

# tested this - it's broken somehow because it didn't work

#Subfunction called by main script below
function NewAceGroup($group, $permission) {
    # Get SID of the group
    $account = [WMI] "\\$env:ComputerName\root\cimv2:Win32_Group.Name='$group',Domain='$env:Userdomain'"
    $accountSID = [WMI] "\\$env:ComputerName\root\cimv2:Win32_SID.SID='$($account.sid)'"

    # Create the trustee object for the group
    $trustee = ([WMIClass] "\\$env:ComputerName\root\cimv2:Win32_Trustee").CreateInstance()
    $trustee.Domain = $env:Userdomain
    $trustee.Name = $group
    $trustee.SID = $accountSID.BinaryRepresentation
    
    # Create and setup access control list object
    $ace = ([WMIClass] "\\$env:ComputerName\root\cimv2:Win32_ACE").CreateInstance()
    $ace.AccessMask = $permission
    $ace.AceFlags = 0
    $ace.AceType = 0
    $ace.Trustee = $trustee
    
    #return value:
    $ace
}

# main script

#variables:
$activation = 25 # Execute | Activate_Local | Activate_Remote
$groups = "Domain Users","Domain Computers","Domain Controllers"

#setup
$AE = Get-WMIObject -Class Win32_DCOMApplicationSetting -Filter 'Description="AutoEnroll"' -EnableAllPrivileges
$perm = $AE.GetLaunchSecurityDescriptor().Descriptor

#add group permissions to local $perm object
foreach ($group in $groups) {
$ace = $perm.Dacl | Where {$_.Trustee.Name -eq $group}
if ($ace) { $ace.AccessMask = $activation }                             # Edit existing ace
else { $perm.Dacl += NewAceGroup($group,$activation) }    # Create new ace
}

#publish back
$AE.SetLaunchSecurityDescriptor($perm)

################################################