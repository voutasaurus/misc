// do while game
var legal = function (move,state) {
    if (move < 0 || move > 8) {
        return false; // not a valid space
    }
    if (state[move] === 0) {
        return true; // space is free
    }
    return false; // space is not free
}

var round = function (state) {
    // player 1 - user
    move = prompt("Your move");
    if (legal(move, state)) {
        state[move] = 1;
    }
    // player 2 - computer chooses randomly
    var valid = [];
    for (var move = 0; move < 9; i++) {
        if (state[move] === 0) {
            valid.push(move);    
        }
    }
    // Math.floor(valid.length * Math.random())
    state[valid[0]] = 2; // first valid move
    return state;
}

var row = function (play, state) {
    for (var x = 0; x < 9; x +=3) {
        if (state[0+x] === play && state[1+x] === play && state[2+x] === play) {
            return true;
        }  
    }
    return false;
}

var col = function (play, state) {
    for (var i = 0; i < 3; i++) {
        if (state[0+i] === play && state[3+i] === play && state[6+i] === play) {
            return true;
        }  
    }
    return false;
}

var diag = function (play, state) {
    if (state[4] != play) {
        return false;
    }
    if (state[0] === play && state[8] === play) {
        return true;
    }
    if (state[0] === play && state[8] === play) {
        return true;
    }
    return false;
}

var winner = function (state) {
    if (row(1,state) || col(1,state) || diag(1,state)) {
        return "Player 1";
    }
    if (row(2,state) || col(2,state) || diag(2,state)) {
        return "Player 2";
    }
    if (state.indexOf(0) === -1) { // no valid moves
        return "draw";
    }
    return "nobody";
}

var game = function () {
    var state = [0,0,0,0,0,0,0,0,0];
    var win = "nobody"
    do {
        state = round(state);
        console.log(state);
        win = winner(state);
    } while (win === "nobody");
    
    if (win != "draw") {
        console.log(win + " wins!");
        return;
    }

    console.log("It's a draw.");
    return;    
}

game();